-- Import plugins
log = require('plugins/Log')
shack = require('plugins/Shack')
stateSwitcher = require('plugins/StateSwitcher')
util = require('lib/util')

-- Clears state engine
stateSwitcher.clear()

-- FPS graph math functionn
util.fpsMath()

-- Check for key presses
function love.keypressed(key)
  -- Debug graph toggle
  util.fpsKeyToggle(key)
  -- Press space to start playing
  util.switchState(key, "space", "play")
end

-- Update function
function love.update(dt)
  util.fpsUpdate(dt)
end

-- Love2D Draw function
function love.draw()
  -- Draw title text
  util.setColor(255, 255, 255, 255)
  util.setRobotoFont(32)
  util.printText("Game Title", util.getWindowWidth() / 2, util.getWindowHeight() / 2)

  -- Toggle Debug Graph data
  if(drawDebugGraphToggle == true) then
    util.drawDebugGraph()
  end
end