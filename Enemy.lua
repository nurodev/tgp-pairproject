local imageFile
local animationFrames = {}
local currentFrame
local enemyState
local enemyHealth
local enemyXPosition
local enemyYPosition
enemy = {}

function enemy.enemySetup(enemyType, enemyXLocation, enemyYLocation)
  if enemyType == 0 then
    imageFile = love.graphics.newImage('assets/sprites/spritesheet/greenTroopEnemy.png')
  elseif enemyType == 1 then
    imageFile = love.graphics.newImage('assets/sprites/spritesheet/superTroopEnemy.png')
  elseif enemyType == 2 then
    imageFile = love.graphics.newImage('assets/sprites/spritesheet/greenTankEnemy.png')
  end
  enemyHealth = 3
  enemyXPosition = enemyXLocation
  enemyYPosition = enemyYLocation
  setUpAnimationFrames()
  enemy.currentFrame = animationFrames[1]
  return imageFile

end

-- Loads animations from the sprite sheet into an array so the enemy graphic can be changed based on what direction it was facing
function setUpAnimationFrames()
  for i = 0, 4, 1 do
    if i < 2 then
    animationFrames[i + 1] = love.graphics.newQuad(i * 64, 0,64,64,imageFile:getDimensions())
  else
    animationFrames[i + 1] = love.graphics.newQuad((i - 2) * 64,64,64,64,imageFile:getDimensions())
    end 
  end
end
function enemy.getCurrentFrame(enemyState)
  if enemyState == "dead" then
    enemyXPosition = -200
    enemyYPosition = -200
  elseif enemyState == "right" then   
  enemy.currentFrame = animationFrames[1]
  enemyXPosition = enemyXPosition + 2
  elseif enemyState == "up" then    
  enemy.currentFrame = animationFrames[2]
  enemyYPosition = enemyYPosition - 2
  elseif enemyState == "left" then     
  enemy.currentFrame = animationFrames [3]
  enemyXPosition = enemyXPosition - 2
  elseif enemyState == "down" then
  enemy.currentFrame = animationFrames [4]
  enemyYPosition = enemyYPosition + 2
  end

  return enemy.currentFrame
end
function enemy.takeDamage()
  enemyHealth = enemyHealth - 1
  if enemyHealth <= 0 then
    enemyState = "dead"
  end
end
return enemy