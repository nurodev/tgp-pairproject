function love.conf(c)
    -- Sets window title
    c.title = "Isometric Racer"

    -- Set to true to enable console on launch
    c.console = true

    local window = c.screen or c.window -- love 0.9 renamed "screen" to "window"
    window.width = 1920 -- Set window width to 1920
    window.height = 1080 -- Set window height to 1080
    window.fullscreen = true -- Set window to fullscreen
    window.borderless = true -- Set window to borderless
    window.vsync = false -- Disable V-Sync
    window.highdpi = true -- Enable High DPI mode
    window.msaa = 4 -- Sets msaa sampling to 4
end