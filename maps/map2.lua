local tileString = [[
  #                           
  #                           
  #                           
  ########################    
                         #    
                         #    
                         #    
                         #    
                         #    
            ##############    
            #                 
     ########                 
     #                        
     #                        
     ################         
                    #         
                    #         
]]

-- Define tile types(background tiles): 
-- string identifier, x, y spritesheet position
local quadInfo = { 
  { ' ', (11*64), (10*64) }, -- Sand
  { '#', (21*64), (6*64) }, -- Rock
}

-- Define entity type
-- Identifier, spritesheet x, y position
local entityInfo = {
  -- Weapons
  { 'single_cannon', (19*64), 640 }, -- single rocket cannon
  { 'double_cannon', (20*64), 640 }, -- double cannon turret
  { 'single_rocket', (21*64), (8*64) }, -- Single rocket turret
  { 'double_rocket', (22*64), (8*64) }, -- Double rocket turret

  -- Extras
  { 'small_grass', (15*64), (5*64) }, -- Small grass
  { 'large_grass', (16*64), (5*64) }, -- Large grass
  { 'small_rock', (20*64), (5*64) }, -- Small rock
  { 'medium_rock', (22*64), (5*64) }, -- Medium rock
  { 'large_rock', (21*64), (5*64) }, -- Large rock
}

-- Define entities in world
-- Entity name, grid x, y position
local entities = {
  {'single_cannon', 1, 1},
  {'double_cannon', 1, 1},
  {'single_rocket', 1, 1},
  {'double_rocket', 1, 1},
  {'small_grass', 1, 1},
  {'large_grass', 1, 1},
  {'small_rock', 1, 1},
  {'medium_rock', 1, 1},
  {'large_rock', 1, 1},
}

-- Final export 
NewMap(64,64,'assets/sprites/towerDefense.png', tileString, quadInfo, entityInfo, entities)