  -- Import plugins
debugGraph = require('plugins/debugGraph')
log = require('plugins/Log')
stateSwitcher = require('plugins/StateSwitcher')
enemy = require('../enemy')
require('lib/mapLoader') 
 
util = {}
 
-- Love2D functions
function love.load()
    -- Logging
    log.info("[UTIL]: Initializing...")
 
    -- Load debug graph data
    drawDebugGraphToggle = false
    fpsGraph = debugGraph:new('fps', 10, 10, 335)
    memGraph = debugGraph:new('mem', 10, 40, 335)
    dtGraph = debugGraph:new('custom', 10, 70, 335)
    
    -- Load map 1 as first map
    LoadMap('maps/map1.lua') 

    -- World variables
    spriteSheetWidth = 2944 
    spriteSheetHeight = 1664 
    --spriteSheetSprite = love.graphics.newImage('assets/sprites/spritesheet/towerDefense_tilesheet@2.png') 
 
    spriteSheet = {} 
 
    for x=1,spriteSheetWidth do 
      spriteSheet[x] = {} 
      for y=1,spriteSheetHeight do 
        spriteSheet[x][y] = love.math.random(0,3) 
      end 
    end 
   
    spriteSheetX = 1 
    spriteSheetY = 1 
    tilesDisplayWidth = 26 
    tilesDisplayHeight = 20 
   
    zoomX = 1 
    zoomY = 1 
 
    -- Enemy variables
    util.enemyGraphic = util.enemySetup(0, enemy)
    util.enemyAnimationFrame = util.getEnemyAnimationFrame("SW down")
 
    -- Sounds
    --backgroundMusic = love.audio.newSource('assets/sounds/backgroundMusic.ogg')
end
 
-- Gets window width
function util.getWindowWidth()
    return love.graphics.getWidth()
end
 
-- Gets window Height
function util.getWindowHeight()
    return love.graphics.getHeight()
end
 -- Gets image width
function util.getImageWidth(image)
    return image:getWidth()
end
 
-- Gets image Height
function util.getImageHeight(image)
    return image:getHeight()
end
 
-- Set color
function util.setColor(r, g, b, a)
  return love.graphics.setColor(r, g, b, a)
end
 
-- Set font to Roboto
function util.setRobotoFont(fontSize)
  return love.graphics.setFont(love.graphics.newFont("assets/fonts/Roboto-Medium.ttf",fontSize))
end
 
-- Print text to screen
function util.printText(text, x, y)
  return love.graphics.print(text, x, y)
end

--load enemy sprite sheet
function util.enemySetup(enemyType)
  return enemy.enemySetup(enemyType)
end

--returns the quad on the spritesheet to be drawn
function util.getEnemyAnimationFrame(carState)
  return enemy.getCurrentFrame(carState)
end

-- Required for debug graph
-- Returns 'n' rounded to the nearest 'deci'th (defaulting whole numbers). 
function util.fpsMath()
  function math.round(n, deci)
    deci = 10^(deci or 0)
    return math.floor(n*deci+.5) / deci
  end
end
 
-- FPS toggle
function util.fpsKeyToggle(key)
  if (key == "f12") then
    -- Logging
    log.info("[UTIL]: Toggling debug graph")
    if (drawDebugGraphToggle == false) then
      drawDebugGraphToggle = true
    elseif (drawDebugGraphToggle == true) then
      drawDebugGraphToggle = false
    end
  end
end
 
-- FPS update
function util.fpsUpdate(dt)
  -- Update the graphs
  fpsGraph:update(dt)
  memGraph:update(dt)
 
  -- Update our custom graph
  dtGraph:update(dt, math.floor(dt * 1000))
  dtGraph.label = 'DT: ' ..  math.round(dt, 4)
end
 
-- Switch state
function util.switchState(key, inputKey, stateToSwitchTo)
  if (key == inputKey) then
    log.info("[UTIL]: Switching state:", stateToSwitchTo)
    stateSwitcher.switch(stateToSwitchTo)
  end
end
 
-- Draws debug graph
function util.drawDebugGraph()
  fpsGraph:draw()
  memGraph:draw()
  dtGraph:draw()
end
 
-- Collision checker function
function util.checkCollision(x1,y1,w1,h1, x2,y2,w2,h2)
  x1 = x1 - w1/2
  y1 = y1 - h1/2
  x2 = x2 - w2/2
  y2 = y2 - h2/2
  return x1 < x2+w2 and
  x2 < x1+w1 and
  y1 < y2+h2 and
  y2 < y1+h1
end
 
return util