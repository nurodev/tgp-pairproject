    -- Import plugins
log = require('plugins/Log')
shack = require('plugins/Shack')
stateSwitcher = require('plugins/StateSwitcher')
util = require('lib/util')
require('lib/mapLoader')

-- FPS graph math functionn
util.fpsMath()
 
-- Check for key presses
function love.keypressed(key)
    -- Debug graph toggle
    util.fpsKeyToggle(key)

    -- Debug press escape to close game
    if (key == "escape") then
        -- Logging
        log.info("[UTIL]: Quitting game")
        love.event.quit()
    end
    
    if (key == "w") then
        util.getCurrentFrame = "up"
    elseif (key == "a") then
        util.getCurrentFrame = "left"
    elseif (key == "d") then
        util.getCurrentFrame = "right"
    elseif (key == "s") then
        util.getCurrentFrame = "down" 
    end

    util.setColor(255, 255, 255, 255)
    love.graphics.draw(util.enemyGraphic, util.enemyAnimationFrame, 400, 400)
    
    if(key == "1") then
        LoadMap('maps/map1.lua') 
    end
    if(key == "2") then
        LoadMap('maps/map2.lua') 
    end
    if(key == "3") then
        LoadMap('maps/map3.lua') 
    end
end
 
-- Update function
function love.update(dt)
    util.fpsUpdate(dt)
end
 
-- Love2D Draw function
function love.draw()
    -- Draw map
    DrawMap()

    -- Toggle Debug Graph data
    if(drawDebugGraphToggle == true) then
        util.drawDebugGraph()
    end
end